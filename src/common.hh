#ifndef __BOWSER__COMMON__GUARD__
#define __BOWSER__COMMON__GUARD__

#include <string>


const ::std::string BROWSER_NAME    = "bowser";
const ::std::string BROWSER_VERSION = "v1.0";

#endif//__BOWSER__COMMON__GUARD__
